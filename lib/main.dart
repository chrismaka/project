import 'package:flutter/material.dart';

void main() {
  runApp(const TabBarDemo());
}

class TabBarDemo extends StatelessWidget {
  const TabBarDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.red,
            bottom: const TabBar(
              indicatorColor: Colors.yellow,
              tabs: [
                Tab(
                  child: Text('Home'),
                ),
                Tab(
                  child: Text('Message'),
                ),
                Tab(
                  child: Text('Settings'),
                ),
                Tab(
                  child: Text('Login'),
                ),
              ],
            ),
            title: const Text('Vision'),
          ),
          body: TabBarView(
            children: [
              homePage(),
              notificationPage(),
              notificationSettingsPage(),
              loginPage(),
            ],
          ),
        ),
      ),
    );
  }

  Widget homePage() {
    return Center(child: Text('Home page here'));
  }

  Widget notificationPage() {
    return Center(child: Text('notification page here'));
  }

  Widget notificationSettingsPage() {
    return Center(child: Text('notification settings page here'));
  }

  Widget loginPage() {
    return Center(child: Text('login page here'));
  }
}
